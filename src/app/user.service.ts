import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = 'https://flask-api-stagge.herokuapp.com/api/user';

  constructor(
    private http: HttpClient
  ) { }

  get() {
    return this.http.get(this.url);
  }

  post(postData) {
    this.http.post<any>(this.url, postData).subscribe(data => {
        console.log(data);
    })
  }
}

