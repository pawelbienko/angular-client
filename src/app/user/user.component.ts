import { UserService } from '../user.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

export interface User {
  username: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
  userForm;
  users;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
  ) {
    this.userForm = this.formBuilder.group({
      username: '',
      email: '',
      password: '',
      firstname: '',
      lastname: ''
    });
  }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.users = this.userService.get();
  }

  onSubmit(data: User) {
    this.userService.post(data);
  }

  refresh(){
    this.getUser();
  }
}
